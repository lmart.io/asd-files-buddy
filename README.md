
# Ableton asd files cleaner

You love working with ableton, but hates all those asd files generated on the fly ?
You know those are needed for analysis purposes but still want to clean them up after a while ?
Yup this script is your friend from now on.
The asd buddy is a python cli program that will help you list and delete asd analisys files when needed.
Need it to run on a frequency basis ? implement the script in a cron job ! 


## Usage/Examples

```python
python3 asd_buddy.py list /path/to/my/audio-things
python3 asd_buddy.py delete /path/to/my/audio-things
```

## Installation

You just need python3 to run it.

## Commands Reference

| command          | actions                                                               |
| ----------------- | ------------------------------------------------------------------ |
| list | will list all the '.asd' files found in the current working directory |
| delete | will delete all the '.asd' files found in the current working directory |
| list /path/to/my/audio-things |will list all the '.asd' files found in the '/path/to/my/audio-things' dir |
| delete /path/to/my/audio-things | will delete all the '.asd' files found in the '/path/to/my/audio-things' dir|
| --help | will display the help pannel |

